import {CLEAR_CART, REMOVE_GAME, ADD_GAME} from 'actions/cartActions';

export const persistCart = store => next => action => {
  let result = next(action);
  switch (action.type) {
    case CLEAR_CART:
    case ADD_GAME:
    case REMOVE_GAME:
      sessionStorage.setItem('gogCart', JSON.stringify(store.getState().cart));
      return result;
    default:
      return result;
  }
};