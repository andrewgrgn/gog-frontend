import {createStore, combineReducers, applyMiddleware} from 'redux';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import {persistCart} from 'middlewares/cartPersistMiddleware';
import {cart} from 'reducers/cartReducer';
import {app} from 'reducers/appReducer';

/* Initializing cart store from saved session storage */
let gogCart = {
  games: [
    {
      "id": 5,
      "title": "Assassin's Creed Director's Cut",
      "price": 9.99,
      "discount": 0,
      "owned": false,
      "thumbnail": "/assets/img/progressive/assassins_creed.jpg"
    },
    {
      "id": 3,
      "title": "The Settlers 2: Gold Edition",
      "price": 5.99,
      "discount": 0,
      "owned": false,
      "thumbnail": "/assets/img/progressive/the_settlers_2.jpg"
    }
  ]
};

try {
  if(sessionStorage.getItem('gogCart')) {
    gogCart = JSON.parse(sessionStorage.getItem('gogCart'));
  }
} catch (e) {
  console.error("Error reading session storage. Make sure your browser support session storage. If error still exists contact technical support.");
}

const client = axios.create({ //all axios can be used, shown in axios documentation
  baseURL:'/',
  responseType: 'json'
});


export const store = createStore(
  combineReducers({cart: cart, app: app}),
  {cart: gogCart, app: {games: [], gameOfTheWeek: null}},
  applyMiddleware(
    persistCart,
    axiosMiddleware(client)
  )
);