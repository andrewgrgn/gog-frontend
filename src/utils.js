export function countGamePrice(price, discount) {
  return (1.0 - discount) * price;
}

export function sumGamesPrice(games) {
  return games.map(item => countGamePrice(item.price, item.discount)).reduce((prev, next) => prev + next, 0).toFixed(2);
}