/*
 * action types
 */
export const GET_HOMEPAGE_DATA = 'GET_HOMEPAGE_DATA';

/*
 * action creators
 */

export function getHomePageData() {
  return {
    type: 'GET_HOMEPAGE_DATA',
    payload: {
      request: {
        url: '/data/games.json'
      }
    }
  }
}