/*
 * action types
 */
export const CLEAR_CART = 'CLEAR_CART';
export const REMOVE_GAME = 'REMOVE_GAME';
export const ADD_GAME = 'ADD_GAME';

/*
 * action creators
 */

export function clearCart() {
  return { type: CLEAR_CART }
}

export function removeGame(gameId) {
  return { type: REMOVE_GAME, gameId: gameId }
}

export function addGame(gameData) {
  return { type: ADD_GAME, gameData: gameData }
}