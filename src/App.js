import React, {Component} from 'react';
import Header from './components/Header/Header';
import GameSpot from './components/GameSpot/GameSpot';
import {connect} from 'react-redux';
import {getHomePageData} from 'actions/appActions';

class App extends Component {
  componentDidMount() {
    this.props.dispatch(getHomePageData());
  }


  render() {
    return (
      <div id="app">
        <Header />
        <GameSpot title="Game of the week"/>
      </div>
    );
  }
}

App = connect()(App);

export default App;
