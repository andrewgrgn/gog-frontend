import React, {Component} from 'react';
import Cart from './Cart/Cart';

class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="header-content">
          <div className="header-container container">
            <a href="#" className="header-logo">
              <img className="header-logo-image" src="/assets/img/logo.png" alt="GOG Logo"/>
            </a>
            <Cart gamesInCart={[
              {
                thumbnailUrl: '/assets/img/progressive/assassins_creed.jpg',
                name: "Assassin's Creed: Director's Cut Edition",
                price: 9.99,
                currency: 'USD'
              },
              {
                thumbnailUrl: '/assets/img/progressive/the_settlers_2.jpg',
                name: "The Settlers 2: Gold Edition",
                price: 5.99,
                currency: 'USD'
              }
            ]} />
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
