import React, {Component} from 'react';
import CartHeader from './CartHeader';
import CartList from './CartList';
import {connect} from 'react-redux';
import {sumGamesPrice} from 'utils';
import {clearCart} from 'actions/cartActions';

class Cart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      gamesCount: props.gamesInCart ? props.gamesInCart.length : 0,
      gamesTotalPrice: props.gamesInCart ? sumGamesPrice(props.gamesInCart) : 0
    };
  }

  onClick = () => {
    this.setState({
        open: !this.state.open
      }
    );
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      gamesCount: nextProps.gamesInCart ? nextProps.gamesInCart.length : 0,
      gamesTotalPrice: nextProps.gamesInCart ? sumGamesPrice(nextProps.gamesInCart) : 0
    });
  }

  render() {
    const {open, gamesCount, gamesTotalPrice} = this.state;
    const {gamesInCart, dispatch} = this.props;

    return (
      <div className={`cart ${open ? "active" : ""}`}>
        <div className="cart-toggle" onClick={this.onClick}>
          <div className="cart-toggle-badge">{ gamesCount }</div>
        </div>
        <div className="cart-content-wrapper">
          <div className="cart-content">
            <CartHeader gamesCount={gamesCount} totalPrice={gamesTotalPrice} onClear={() => dispatch(clearCart())}/>
            {gamesCount > 0 && <CartList games={gamesInCart} dispatch={dispatch} />}
          </div>
        </div>
      </div>
    );
  }
}

Cart = connect(
  store => {
    return {
      gamesInCart: store.cart.games
    }
  }
)(Cart);

export default Cart;
