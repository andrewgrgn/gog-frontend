import React from 'react';
import CartListItem from './CartListItem';
import {removeGame} from 'actions/cartActions';

export default (props) => {
  return (
    <div className="cart-list-wrapper">
      {props.games
        .map((item, index) => <CartListItem key={index} item={item} onRemove={() => props.dispatch(removeGame(item.id))} />)}
    </div>
  )
}