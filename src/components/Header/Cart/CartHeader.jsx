import React from 'react';

export default (props) => {
  const {gamesCount, totalPrice} = props;

  return (
    <div className="cart-content-header">
      <span className="cart-content-header-count">
        {gamesCount
          ? `${gamesCount} item${gamesCount > 1 ? 's' : ''} in cart`
          : `Cart is empty`
        }
        </span>
      {gamesCount > 0 && <span className="cart-content-header-totalPrice">{`$ ${totalPrice}`}</span>}
      {gamesCount > 0 && <button onMouseDown={props.onClear} className="btn btn-big btn-purple cart-content-header-actionButton">Clear cart</button>}
    </div>
  )
}