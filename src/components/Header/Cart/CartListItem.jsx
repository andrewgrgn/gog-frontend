import React from 'react';
import {countGamePrice} from 'utils';

export default (props) => {
  const game = props.item;

  return (
    <div className="cart-list-item">
      <img className="cart-list-item-image" src={game.thumbnail} alt={game.title} title={game.title}/>
      <div className="cart-list-item-title">
        <div className="cart-list-item-title-text">
          <span>{game.title}</span>
        </div>
        <button onMouseDown={props.onRemove} className="cart-list-item-remove">Remove</button>
      </div>
      <div className="cart-list-item-price">
        {`$ ${countGamePrice(game.price, game.discount)}`}
      </div>
    </div>
  )
}