import React, {Component} from 'react';
import GameList from './GamesList/GameList';
import SecretKey from 'components/SecretKey';
import {connect} from 'react-redux';

class GameSpot extends Component {
  constructor() {
    super();

    this.state = {
      secret: true
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props !== nextProps || this.state !== nextState;
  }

  onSecretKeyClick = () => {
    this.setState({
      secret: !this.state.secret
    });
  };

  render() {
    const {title, gameOfTheWeek, games, dispatch} = this.props;
    const {secret} = this.state;

    return (
      <section className="gamespot">
        <div className="container">
          <h3 className="gamespot-title">{title} <SecretKey onSecretKeyClick={this.onSecretKeyClick} /></h3>
          <div className="gamespot-content">
            <button id="secret-button" className="btn btn-big btn-green-gradient">It's supposed to be a secret</button>
            {gameOfTheWeek &&
            <img className={`gamespot-gameOfTheWeek ${!secret ? 'hide' : ''}`} src={gameOfTheWeek.poster} alt={gameOfTheWeek.title} title={gameOfTheWeek.title} />}
          </div>
        </div>
        <div className="container">
          <GameList dispatch={dispatch} games={games} />
        </div>
      </section>
    )
  }
}

GameSpot = connect(
  store => {
    /* Set information about games in cart */
    const inCartGameIds = store.cart.games.map(item => item.id);
    const games = store.app.games.map(item => {return {...item, inCart: inCartGameIds.includes(item.id)}});
    return {
      gameOfTheWeek: store.app.gameOfTheWeek,
      games: games
    }
  }
)(GameSpot);

export default GameSpot;