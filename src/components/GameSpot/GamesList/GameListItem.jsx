import React from 'react';
import {countGamePrice} from 'utils';
import Dotdotdot from 'react-dotdotdot';

export default (props) => {
  const {game, onRemove, onAdd} = props;
  const STATE = {
    isDiscounted: game.discount > 0 && !game.owned,
    owned: game.owned,
    canBuy: !game.owned && !game.inCart,
    inCart: game.inCart
  };

  const handleClick = () => {
    if(STATE.inCart) {
      onRemove(game.id);
    } else if(STATE.canBuy) {
      onAdd(game);
    }

    return false;
  };


  return (
    <div className={`gamelist-item ${game.owned ? 'owned' : ''}`}>
      <img className="gamelist-item-image" src={game.thumbnail} alt={game.title} title={game.title}/>
      <div className="gamelist-item-title">
        <span className="gamelist-item-title-text"><Dotdotdot clamp={2}>{game.title}</Dotdotdot></span>
      </div>
      <div className="gamelist-item-actions">

        {STATE.isDiscounted &&
        <button
          title={`$ ${(game.discount * game.price).toFixed(2)} OFF`}
          className="btn btn-green gamelist-item-actions-discount">
          {`-${parseInt(game.discount*100, 10)}%`}
          </button>}

        {(STATE.inCart || STATE.canBuy) &&
        <button className="btn gamelist-item-actions-button" onMouseDown={handleClick}>
          <span className="hovered">{STATE.canBuy ? 'Add' : 'Remove'}</span>
          <span className="unhovered">
            {STATE.canBuy ? `$ ${countGamePrice(game.price, game.discount).toFixed(2)}` : 'In cart'}
            </span>
        </button>}

        {STATE.owned && <button className="btn btn-disabled gamelist-item-actions-button">Owned</button>}
      </div>
    </div>
  )
}