import React, {Component} from 'react';
import GameListItem from './GameListItem';
import {removeGame, addGame} from 'actions/cartActions';

class GameList extends Component {

  shouldComponentUpdate(nextProps) {
    return this.props !== nextProps;
  }

  onRemove = gameId => {
    this.props.dispatch(removeGame(gameId));
  };

  onAdd = gameData => {
    this.props.dispatch(addGame(gameData));
  };

  render() {
    const {games} = this.props;

    return (
      <div className="gamelist">
        {games.map((item, index) =>
          <GameListItem onRemove={this.onRemove} onAdd={this.onAdd} game={item} key={index} />
        )}
      </div>
    )
  }
}


export default GameList;