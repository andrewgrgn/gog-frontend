import {GET_HOMEPAGE_DATA} from 'actions/appActions';

const appInitialState = {};

export function app(state = appInitialState, action) {
  switch (action.type) {
    case `${GET_HOMEPAGE_DATA}_SUCCESS`:
      return action.payload.data;
    default:
      return state
  }
}