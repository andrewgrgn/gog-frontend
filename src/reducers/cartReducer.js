import {CLEAR_CART, REMOVE_GAME, ADD_GAME} from 'actions/cartActions';

const cartInitialState = {game: []};

export function cart(state = cartInitialState, action) {
  switch (action.type) {
    case CLEAR_CART:
      return {
        ...state,
        games: []
      };
    case REMOVE_GAME:
      return {
        ...state,
        games: state.games.filter(item => item.id !== action.gameId)
      };
    case ADD_GAME:
      if(state.games.includes(action.gameData)) {
        return state;
      } else {
        return {
          ...state,
          games: [...state.games, action.gameData]
        };
      }
    default:
      return state
  }
}